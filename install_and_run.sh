#!/usr/bin/env bash

if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi
echo 'Update linux depencies' &&
sudo apt-get -yy -qq update &&
echo 'Install needed packages' &&
sudo apt-get -yy -qq install python3-pip ufw screen &&
echo 'Install needed python packages' &&
sudo xargs -a requirements.txt -n 1 sudo pip3 -q install &&

echo 'Kill previously started API' &&
sudo screen -X -S UfwApi quit || true &&

echo 'Start UfwApi in a background screen called UfwApi' &&
sudo screen -S UfwApi -dm bash -c 'sudo python3 app.py;exec sh'