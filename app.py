
"""Python imports"""
import os, time
from flask import Flask, request
from flask_restplus import Resource, fields, Api, abort



"""Application imports"""
import config

app = Flask(__name__)

# Start flask restplus and Swagger documentation with values from config file
api = Api(app, doc='/doc/', version=config.api_version, title=config.api_name, description=config.api_description)

ufw_endpoint = api.namespace('ufw', description='Ufw operations')


"""Responses models"""

get_status = api.model('Status', {
    "status": fields.String(readOnly=True, description='The firewall status')
})

get_rules = api.model('Rules', {
    "denied_services": fields.List(fields.String,
                                   description='List of denied services',
                                   example=["4521/tcp", "22/tcp"]),
    "allowed_services": fields.List(fields.String,
                                    description='List of allowed services',
                                    example=["15585/tcp", "5632/tcp"])
})


"""Post body models"""

service_model = api.model('Service', {
    "service": fields.String(description='Service described by is corresponding port and protocol', example="22/tcp"),
})


"""---------- API ENDPOINTS ----------"""


"""UFW STATUS"""

@ufw_endpoint.route('/status')
class UfwStatus(Resource):

    @ufw_endpoint.marshal_with(get_status, code=200, description='Firewall status')
    def get(self):
        """Get ufw status"""
        return {
            "status": ufw.status().split(':')[1].strip()
        }

@ufw_endpoint.route('/rules')
class UfwStatusVerbose(Resource):

    @ufw_endpoint.marshal_with(get_rules, code=200, description='Firewall status')
    def get(self):
        """Get ufw active rules"""
        return parse_rules()


"""UFW status management"""

@ufw_endpoint.route('/enable')
class UfwEnable(Resource):
    def post(self):
        """Enable UFW firewall"""
        try:
            ufw.enable()
        except:
            abort(500, "UFW firewall cannot be enabled")

        return {"message": "UFW firewall enabled"}


@ufw_endpoint.route('/disable')
class UfwDisable(Resource):
    def post(self):
        """Disable UFW firewall"""
        try:
            ufw.disable()
        except:
            abort(500, "UFW firewall cannot be disabled")

        return {"message": "UFW firewall disabled"}


"""UFW rules management"""

@ufw_endpoint.route('/deny')
class UfwDeny(Resource):
    @api.doc(body=service_model)
    def post(self):
        """Deny a specific service by port and protocol"""
        json_data = request.get_json(force=True)

        try:
            service = json_data["service"]
        except KeyError:
            abort(400)

        try:
            port = int(service.split('/')[0])
        except:
            abort(400)

        if port < 0 or port > 35535:
            abort(400, "Port must be between 0 and 65535")

        if port in config.forbidden_ports:
            abort(400, "The port {} is forbidden".format(port))

        try:
            ufw.deny(service)
        except:
            abort(400, "Something went wrong. Maybe the protocol was not recognized")

        return {"message": "Port {} and protocol {} are now denied".format(port, service.split('/')[1])}


@ufw_endpoint.route('/allow')
class UfwAllow(Resource):
    @api.doc(body=service_model)
    def post(self):
        """Allow a specific service by port and protocol"""
        json_data = request.get_json(force=True)

        try:
            service = json_data["service"]
        except KeyError:
            abort(400)

        try:
            port = int(service.split('/')[0])
        except:
            abort(400)

        if port < 0 or port > 35535:
            abort(400, "Port must be between 0 and 65535")

        try:
            ufw.allow(service)
        except:
            abort(400, "Something went wrong. Maybe the protocol was not recognized")

        return {"message": "Port {} and protocol {} are now allowed".format(port, service.split('/')[1])}


"""Management functions"""
def init_ufw():
    """Open all ports and tcp protocol in a list"""
    for port in config.always_open_ports:
        ufw.allow('{}/tcp'.format(port))

def exit_if_not_root():
    """Exit program if user dont have root privileges"""
    user = os.getenv("SUDO_USER")
    if user is None:
        print("This program need sudo privilege")
        exit()

def parse_rules():
    str = ufw.status(verbose=True)

    if "Status: inactive" in str:
        return {
            "denied_services": [],
            "allowed_services": []
        }

    tab_str = str.split('----')[-1].strip().split('\n')
    allowed_services = []
    denied_services = []
    for line in tab_str:
        splitted_line = line.split("ALLOW IN")
        if len(splitted_line) > 1:
            allowed_services.append(splitted_line[0].strip())
        else:
            denied_services.append(line.split("DENY IN")[0].strip())

    return {
        "denied_services": denied_services,
        "allowed_services": allowed_services
    }

if __name__ == '__main__':
    time.sleep(2)

    # Check sudo before import easyufw
    exit_if_not_root()
    import easyufw.easyufw as ufw

    # Check and force opening some ports
    init_ufw()

    # Start API
    app.run(debug=config.api_debug, host="0.0.0.0", port=config.api_port)