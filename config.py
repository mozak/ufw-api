

# API configuration
api_debug = True
api_port = 5000

api_version = "1.0"
api_name = "SSICLOPS FIREWALL API"
api_description = "API to manage SSICLOPS Firewall"


# Forbidden ports
forbidden_ports = [22]
forbidden_ports.append(api_port)

always_open_ports = [22]
always_open_ports.append(api_port)