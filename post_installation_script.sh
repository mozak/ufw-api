#!/usr/bin/env bash

sudo apt update
sudo apt install git
git clone https://gitlab.com/mozak/ufw-api.git
cd ufw-api
chmod +x install_and_run.sh
./install_and_run.sh